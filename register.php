<?php

	$errors = array();
	$signup = false;

	if( $_SERVER[ 'REQUEST_METHOD' ] == 'POST' ) {





			$dsn = 'mysql:dbname=internet;host=localhost;port=3306';
			$username = 'root';
			$password = '';
			try {
				$db = new PDO($dsn,$username,$password);
                $db->exec( "SET CHARACTER SET utf8" );
} catch( PDOException $e ) {
die( 'رخداد خطا در هنگام ارتباط با پایگاه داده:<br>' . $e );
}
$init_level = 1;
$stmt = $db->prepare( "INSERT INTO users (username,email,password,userlevel) VALUES ( ?, ? ,?,?)" );
$stmt->bindValue( 1, $_POST['username'] );
$stmt->bindValue( 2, $_POST['email'] );
$stmt->bindValue( 3, password_hash( $_POST['password'], PASSWORD_BCRYPT ) );
$stmt->bindValue( 4,$init_level);
$stmt->execute();

$signup = true;

}
?>

<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title> Sign up </title>
<link href="https://fonts.googleapis.com/css?family=Roboto:400,700" rel="stylesheet">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<style type="text/css">
	body {
		background: #dfe7e9;
		font-family: 'Roboto', sans-serif;
	}
    .form-control {
		font-size: 16px;
		transition: all 0.4s;
		box-shadow: none;
	}
	.form-control:focus {
        border-color: #5cb85c;
    }
    .form-control, .btn {
        border-radius: 50px;
		outline: none !important;
    }
	.signup-form {
		width: 480px;
    	margin: 0 auto;
		padding: 30px 0;
	}
    .signup-form form {
		border-radius: 5px;
    	margin-bottom: 20px;
        background: #fff;
        box-shadow: 0px 2px 2px rgba(0, 0, 0, 0.3);
        padding: 40px;
    }
	.signup-form a {
		color: #5cb85c;
	}
	.signup-form h2 {
		text-align: center;
		font-size: 34px;
		margin: 10px 0 15px;
	}
	.signup-form .hint-text {
		color: #999;
		text-align: center;
		margin-bottom: 20px;
	}
	.signup-form .form-group {
		margin-bottom: 20px;
	}
    .signup-form .btn {
        font-size: 18px;
		line-height: 26px;
        font-weight: bold;
		text-align: center;
    }
	.signup-btn {
		text-align: center;
		border-color: #AC2BF0;
		transition: all 0.4s;
	}
	.signup-btn:hover {
		background: #AC2BF0;
		opacity: 0.8;
	}
    .or-seperator {
        margin: 50px 0 15px;
        text-align: center;
        border-top: 1px solid #AC2BF0;
    }
    .or-seperator b {
        padding: 0 10px;
		width: 40px;
		height: 40px;
		font-size: 16px;
		text-align: center;
		line-height: 40px;
		background: #fff;
		display: inline-block;
        border: 1px solid #8661FA;
		border-radius: 50%;
        position: relative;
        top: -22px;
        z-index: 1;
    }
    .social-btn .btn {
		color: #fff;
        margin: 10px 0 0 15px;
		font-size: 15px;
		border-radius: 50px;
		font-weight: normal;
		border: none;
		transition: all 0.4s;
    }
	.social-btn .btn:first-child {
		margin-left: 0;
	}
	.social-btn .btn:hover {
		opacity: 0.8;
	}
	.social-btn .btn-primary {
		background: #AC2BF0;
	}
	.social-btn .btn-info {
		background: #AC2BF0;
	}
	.social-btn .btn-danger {
		background: #AC2BF0;
	}
	.social-btn .btn i {
		float: left;
		margin: 3px 10px;
		font-size: 20px;
	}
</style>
</head>
<body>
  <?php if( $signup == false ) : ?>
<div class="signup-form">
    <form method="post">
		<h2>Create  Account</h2>
		<p class="hint-text">Sign up :)</p>

        <div class="form-group">
        	<input type="text" class="form-control input-lg" name="username" placeholder="Username" required="required">
        </div>
		<div class="form-group">
        	<input type="email" class="form-control input-lg" name="email" placeholder="Email Address" required="required">
        </div>
		<div class="form-group">
            <input type="password" class="form-control input-lg" name="password" placeholder="Password" required="required">
        </div>
		<div class="form-group">
            <input type="password" class="form-control input-lg" name="confirm_password" placeholder="Confirm Password" required="required">
        </div>
        <div class="form-group">
            <button type="submit" class="btn btn-success btn-lg btn-block signup-btn">Sign Up</button>
        </div>
    </form>
  <?php else: ?>
      <script>
          window.location.href = 'login.php';

      </script>
      <?php endif; ?>
    <div class="text-center">Already have an account? <a href="login.php">Login here</a></div>
</div>
</body>
</html>
