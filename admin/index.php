<?php
session_start();
if( isset( $_GET[ 'signout' ] ) ) {
		unset( $_SESSION[ 'email' ] );
        Redirect('../login.php', false);
	}
function Redirect($url, $permanent = false)
{
    header('Location: ' . $url, true, $permanent ? 301 : 302);
    exit();
}

	$errors = array();

    $flag = false;

	if( $_SERVER[ 'REQUEST_METHOD' ] == 'POST' ) {
			$dsn = 'mysql:dbname=internet;host=localhost;port=3306';
			$username = 'root';
			$password = '';
			try {
				$db = new PDO($dsn,$username,$password);
                $db->exec( "SET CHARACTER SET utf8" );
} catch( PDOException $e ) {
die( 'رخداد خطا در هنگام ارتباط با پایگاه داده:<br>' . $e );
}

$stmt = $db->prepare( "INSERT INTO quize ( quesion,answer1,answer2,answer3,answer4,level,trueans) VALUES (?,?,?,?,?,?,?)" );
$stmt->bindValue( 1, $_POST['question']);
$stmt->bindValue( 2, $_POST['firstans'] );
$stmt->bindValue( 3, $_POST['secondans']);
$stmt->bindValue( 4, $_POST['thirdans'] );
$stmt->bindValue( 5, $_POST['fourthans'] );
$stmt->bindValue( 6, $_POST['level'] );
$stmt->bindValue( 7, $_POST['truans'] );
$stmt->execute();

$flag = true;

}
?>
<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf8_general_ci">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <!--<link rel="stylesheet" href="https://unpkg.com/bootstrap-material-design@4.1.1/dist/css/bootstrap-material-design.min.css" integrity="sha384-wXznGJNEXNG1NFsbm0ugrLFMQPWswR3lds2VeinahP8N0zJw9VWSopbjv2x7WCvX" crossorigin="anonymous">-->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="css/style.css">
    <link href="css/style_1.css" rel="stylesheet" type="text/css" />

    <title>add question</title>
  
</head>

<body style="background-color:#616161;">
    <?php if( $flag == false ) : ?>
    <nav class="navbar navbar-expand-lg navbar-light fixed-top w-100" style="background-color: #dadada">
        <div class="container-fluid">

            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse w-100" id="navbarSupportedContent">
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item active ml-2">
                        <a class="nav-link" href="../quit.php"><span class="blue-txt">Quit</span><span class="sr-only">(current)</span></a>
                    </li>
                </ul>

            <!--onclick="searchFunction()"-->
<!--            <div id="snackbar">موردی یافت نشد.</div>-->

                <?php if (!isset($_SESSION["email"])) : ?>
                <div class="btn-group mr-2">
                    <a href="/ozviat.php" role="button" class="text-white btn rounded-right blue-btn" style="border-radius:0;">عضویت
                    </a>
                    <a href="/vorud.php" role="button" class="btn rounded-left pt-1 pb-1 purple-btn" style="border-radius:0;">ورود
                    </a>
                </div>


                <?php else: ?>
                <div class="btn-group mr-2">

                    <button type="button" class="text-white btn rounded-right disabled" style="background-color: #8E54F8; border-radius:0;">
                        <?=$_SESSION['email']?>
                    </button>
                </div>
                <?php endif; ?>
            </div>
        </div>
    </nav>



    <div class="w-100 pt-1 pb-1">

        <div class="container-fluid  fh5co_fh5co_bg_contcat bg-white " style="direction:ltr; width: 100%; margin-top: 7%;">

        </div>
    </div>
    <div class="w-100 pt-2 pb-2" id="contact">
        <div class="container bg-white mt-4 mb-5">

            <div class="row pt-3">
                <div class="col-12 text-center contact_margin_svnit ">
                    <div class="fh5co_heading float-right pr-2" style="color: rgb(44,117,152)">
                        <h5>add question
                        </h5>
                    </div>
                </div>
            </div>
            <div class="row pb-4">
                <div class="col-md-6 pr-4">
                    <form method="POST">
											<div class="col-12 py-3">
													<input class="form-control fh5co_contacts_message" placeholder="question" name="question" />
											</div>
											<div class="col-12 py-3">
													<input class="form-control fh5co_contacts_message" placeholder="first answer" name="firstans" />
											</div>
											<div class="col-12 py-3">
													<input class="form-control fh5co_contacts_message" placeholder="second answer" name="secondans" />
											</div>
											<div class="col-12 py-3">
													<input class="form-control fh5co_contacts_message" placeholder="third answer" name="thirdans" />
											</div>
											<div class="col-12 py-3">
													<input class="form-control fh5co_contacts_message" placeholder="fourth answer" name="fourthans" />
											</div>
											<div class="col-12 py-3">
													<input class="form-control fh5co_contacts_message" placeholder="question level" name="level" />
											</div>
											<div class="col-12 py-3">
													<input class="form-control fh5co_contacts_message" placeholder="true answer : A , B , C , D" name="truans" />
											</div>

                        <button class="col-12 py-3 text-center"> <strong>add question</strong> </button>
                    </form>
                </div>
                <div class="col-md-4 align-self-center float-right">
										<img src="../images/download.png" alt="admin">
										 </div>
            </div>

        </div>

    </div>





    <script>
        $(document).ready(function() {
            $('[data-toggle="tooltip"]').tooltip();
        });
    </script>
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="js/bootstrap.min.js"></script>
</body>
<?php else: ?>
<script>
    window.location.href = 'index.php';
</script>
<?php endif; ?>

</html>
