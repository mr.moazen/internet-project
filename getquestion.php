
<?php


session_start();
if( isset($_GET['signout']) ) {
		unset( $_SESSION[ 'email' ] );
    Redirect('login.php', false);
	}
include 'functions.php';
?>




<html>

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />

	<title>PHP Quiz</title>

	<link rel="stylesheet" type="text/css" href="css/style.css" />
</head>

<body>

	<div id="page-wrap">

		<h1>hello to the quizzy :)</h1>

		<form action="grade.php" method="post" id="quiz">
            <ol>
						<?php
						makequestion();
						?>
            </ol>
            <input type="submit" value="Submit Quiz" />

		</form>

	</div>

	<script type="text/javascript">
	var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
	document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
	</script>
	<script type="text/javascript">
	var pageTracker = _gat._getTracker("UA-68528-29");
	pageTracker._initData();
	pageTracker._trackPageview();
	</script>

</body>

</html>
